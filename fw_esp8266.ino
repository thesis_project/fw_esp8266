/*
 *   ESP-01 firmware, built with IotWebConf: an ESP8266/ESP32
 *   non blocking WiFi/AP web configuration library for Arduino.
 *   https://github.com/prampec/IotWebConf 
 *   Requires a custom version of the NTPClient library, available from:
 *   https://git.dotya.ml/2EEEB/NTPClient
 */

#include <MQTT.h>
#include <IotWebConf.h>
#include <NTPClient.h>

#define IOTWEBCONF_DEBUG_DISABLED //disable debug output from esp

// -- Initial name of the Thing. Used e.g. as SSID of the own Access Point.
const char thingName[] = "inenvmon";
// -- Initial password to connect to the Thing, when it creates an own Access Point.
const char wifiInitialApPassword[] = "verynice";
#define STRING_LEN 128
#define NUMBER_LEN 32
// -- Configuration specific key. The value should be modified if config structure was changed.
#define CONFIG_VERSION "inenvmon_1.0"

// -- Callback method declarations.
void wifiConnected();
void configSaved();
boolean formValidator();

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
DNSServer dnsServer;
WebServer server(80);
HTTPUpdateServer httpUpdater;
WiFiClientSecure net;
MQTTClient mqttClient;

char mqttServerValue[STRING_LEN];
char mqttUserNameValue[STRING_LEN];
char mqttUserPasswordValue[STRING_LEN];
char mqttTopicValue[STRING_LEN];
char mqttServerCertFingerprintValue[STRING_LEN];
char ntpServerValue[STRING_LEN];
char ntpUtcOffsetValue[NUMBER_LEN];

// Configurable parameters
IotWebConf iotWebConf(thingName, &dnsServer, &server, wifiInitialApPassword, CONFIG_VERSION);
IotWebConfParameter separator1 = IotWebConfSeparator("MQTT settings");
IotWebConfParameter mqttServerParam = IotWebConfParameter("MQTT server", "mqttServer", mqttServerValue, STRING_LEN);
IotWebConfParameter mqttUserNameParam = IotWebConfParameter("MQTT user", "mqttUser", mqttUserNameValue, STRING_LEN);
IotWebConfParameter mqttUserPasswordParam = IotWebConfParameter("MQTT password", "mqttPass", mqttUserPasswordValue, STRING_LEN, "password");
IotWebConfParameter mqttTopicParam = IotWebConfParameter("MQTT topic","mqttTopic",mqttTopicValue,STRING_LEN);
IotWebConfParameter mqttServerCertFingerprintParam = IotWebConfParameter("MQTT server cert SHA-1 fingerprint","mqttCertFp",mqttServerCertFingerprintValue,STRING_LEN,"text","Leave blank for unsecured connection");
IotWebConfParameter separator2 = IotWebConfSeparator("NTP settings");
IotWebConfParameter ntpServerParam = IotWebConfParameter("NTP server address","ntpServer",ntpServerValue,STRING_LEN,"text","e.g. europe.pool.ntp.org");
IotWebConfParameter ntpUtcOffsetParam = IotWebConfParameter("Time zone (UTC offset)", "ntpUtcOffset", ntpUtcOffsetValue, NUMBER_LEN, "number", "UTC offset in seconds");

boolean needMqttConnect = false;
boolean needReset = false;
unsigned long lastMqttConnectionAttempt = 0;
char incoming[40];
bool commRecvd = false;

void setup() {
    Serial.begin(115200);
    Serial.println();
    Serial.println("Starting up...");

    WiFi.mode(WIFI_AP_STA);  //enable softap mode after startup

    iotWebConf.addParameter(&separator1);
    iotWebConf.addParameter(&mqttServerParam);
    iotWebConf.addParameter(&mqttUserNameParam);
    iotWebConf.addParameter(&mqttUserPasswordParam);
    iotWebConf.addParameter(&mqttTopicParam);
    iotWebConf.addParameter(&mqttServerCertFingerprintParam);
    iotWebConf.addParameter(&separator2);
    iotWebConf.addParameter(&ntpServerParam);
    iotWebConf.addParameter(&ntpUtcOffsetParam);
    iotWebConf.setConfigSavedCallback(&configSaved);
    iotWebConf.setFormValidator(&formValidator);
    iotWebConf.setWifiConnectionCallback(&wifiConnected);
    iotWebConf.setupUpdateServer(&httpUpdater);

    // -- Initializing the configuration.
    boolean validConfig = iotWebConf.init();
    if (!validConfig) {
      mqttServerValue[0] = '\0';
      mqttUserNameValue[0] = '\0';
      mqttUserPasswordValue[0] = '\0';
      mqttTopicValue[0] = '\0';
      mqttServerCertFingerprintValue[0] = '\0';
      ntpServerValue[0] = '\0';
      ntpUtcOffsetValue[0] = '\0';
    }

    timeClient.configure(ntpServerValue, atoi(ntpUtcOffsetValue)); //configure ntpclient with supplied values
    timeClient.setUpdateInterval(30000UL);

    if(mqttServerCertFingerprintValue[0] != '\0') {
        net.setFingerprint(mqttServerCertFingerprintValue);
    }
    else {
       net.setInsecure(); //no ssl if cert is not defined
    }

    // -- Set up required URL handlers on the web server.
    server.on("/", handleRoot);
    server.on("/config", []{ iotWebConf.handleConfig(); });
    server.onNotFound([](){ iotWebConf.handleNotFound(); });

    if(mqttServerCertFingerprintValue[0] != '\0') {
        mqttClient.begin(mqttServerValue, 8883, net); //use mqtts port if cert is defined
    }
    else {
        mqttClient.begin(mqttServerValue, net);
    }
    Serial.println("Ready.");
}

void loop() 
{
    char buf;
    static char i=0;

    while(Serial.available() > 0) {
        buf = Serial.read();
        if(buf != '\n') {
          incoming[i] = buf;
          i++;
        }
        else {
          incoming[i] = '\0';
          commRecvd = true;
        }
    }
    i = 0;      
    if(commRecvd == true){
         if(strcmp("?ntp", incoming) == 0){  //if request for time update received, process time update
            while(!timeClient.update()) { //ntp update
                delay(10); //time sync is critical, wait for succesful update
            };
            Serial.print(">");     //indicate time data
            delay(50);
            Serial.print(timeClient.getYear());
            Serial.print("/");
            Serial.print(timeClient.getMonth());
            Serial.print("/");
            Serial.print(timeClient.getDate());
            Serial.print("/");
            Serial.print(timeClient.getHours());
            Serial.print("/");
            Serial.print(timeClient.getMinutes());
            Serial.print("/");
            Serial.print(timeClient.getSeconds());
            Serial.print('\n');
            delay(1000); //give time to process comm
        }
        else {
            mqttClient.publish(mqttTopicValue,incoming);  //publish whatever came through to the mqtt topic
        };
        commRecvd = false;
    }
    delay(100);

    // -- doLoop should be called as frequently as possible.
    iotWebConf.doLoop();
    mqttClient.loop();

    if (needMqttConnect) {
        if (connectMqtt()) {
            needMqttConnect = false;
        }
    }
    else if ((iotWebConf.getState() == IOTWEBCONF_STATE_ONLINE) && (!mqttClient.connected())) {
        // Serial.println("MQTT reconnect");
        connectMqtt();
    }
    if (needReset) {
        // Serial.println("Rebooting after 1 second.");
        iotWebConf.delay(1000);
        ESP.restart();
    }
}
/**
 * Handle web requests to "/" path.
 */
void handleRoot() {
    // -- Let IotWebConf test and handle captive portal requests.
    if (iotWebConf.handleCaptivePortal()) {
        // -- Captive portal request already served.
        return;
    }
    String s = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/>";
    s += "<title>Inenvmon</title></head><body>Indoor environment monitor.";
    s += "<ul>";
    s += "<li>MQTT server: ";
    s += mqttServerValue;
    s += "<li> Data overview available at ";
    s += "<a href='https://env.astora.gq'>env.astora.gq</a>";
    s += "</ul>";
    s += "Go to <a href='config'>config page</a> to change values.";
    s += "</body></html>\n";

    server.send(200, "text/html", s);
}

void wifiConnected() {
    WiFi.mode(WIFI_STA);  //disable ap after connecting to wifi
    delay(100);
    Serial.println();
    Serial.print("!");
    Serial.print(WiFi.SSID());
    Serial.print(",");
    Serial.print(WiFi.localIP());
    Serial.print("\n");
    delay(100);
    needMqttConnect = true;
}

void configSaved() {
    // Serial.println("Configuration was updated.");
    needReset = true;
}

boolean formValidator() {
    // Serial.println("Validating form.");
    boolean valid = true;

    int l = server.arg(mqttServerParam.getId()).length();
    if (l < 3) {
        mqttServerParam.errorMessage = "Please provide at least 3 characters!";
        valid = false;
    }

    return valid;
}

boolean connectMqtt() {
    unsigned long now = millis();
    if (30000UL > now - lastMqttConnectionAttempt) {
        // Do not repeat within 30 secs.
        return false;
    }
    // Serial.println("Connecting to MQTT server...");
    if (!connectMqttOptions()) {
        lastMqttConnectionAttempt = now;
        return false;
    }
    // Serial.println("Connected!");

    return true;
}

boolean connectMqttOptions() {
    boolean result;
    if (mqttUserPasswordValue[0] != '\0') {
        result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue, mqttUserPasswordValue);
    }
    else if (mqttUserNameValue[0] != '\0') {
        result = mqttClient.connect(iotWebConf.getThingName(), mqttUserNameValue);
    }
    else {
        result = mqttClient.connect(iotWebConf.getThingName());
    }
    
    return result;
}
